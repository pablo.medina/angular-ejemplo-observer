import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { INumerosRequest } from 'src/app/models/numero.model';

@Injectable({
  providedIn: 'root'
})
export class NumerosService {
  private numerosSubject$ = new BehaviorSubject<number[]>([]);

  constructor() { }

  generarNumeros({ cantidad, maximo, entero }: INumerosRequest = { cantidad: 50, maximo: 100, entero: true }): void {
    const numeros: number[] = [];
    for (let i = 0; i < cantidad; i++) {
      const numero = Math.random() * (maximo + 1);
      numeros.push(entero ? Math.floor(numero) : numero);
    }
    console.debug('Números:', numeros);
    this.numerosSubject$.next(numeros);
  }

  get numeros$(): Observable<number[]> {
    return this.numerosSubject$.asObservable();
  }
}
