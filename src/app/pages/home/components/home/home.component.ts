import { Component, OnDestroy, OnInit } from '@angular/core';
import { NumerosService } from 'src/app/core/services/numeros.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  numeros: number[] = [];
  destroy$ = new Subject<void>();

  constructor(
    private numerosService: NumerosService
  ) { }

  ngOnInit(): void {
    //Forma incorrecta de hacer el subscribe en un Observable infinito
    //this.numerosService.numeros$.subscribe(numeros => this.cargarNumeros(numeros));

    //Forma correcta
    this.numerosService.numeros$
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(numeros => this.cargarNumeros(numeros));
  }

  cargarNumeros(numeros: number[]) {
    this.numeros = numeros;
    console.debug('Carga de números OK');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
