import { Component, OnInit } from '@angular/core';
import { NumerosService } from 'src/app/core/services/numeros.service';

const CANT_NUMEROS = 10;
const VALOR_MAXIMO = 100;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    private numerosService: NumerosService
  ) { }

  ngOnInit(): void {
  }

  generarNumeros(cantidad: number): void {
    this.numerosService.generarNumeros({cantidad, maximo: VALOR_MAXIMO, entero: true});
  }

}
